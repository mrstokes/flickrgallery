//
//  FlickrHelper.swift
//  PhotoGallery
//
//  Created by Chris Stokes on 16/03/2020.
//  Copyright © 2020 Chris Stokes. All rights reserved.
//

import Foundation
import Alamofire

struct FlickrObject {
    let tag: String
    let farm: String
    let server: String
    let photoID: String
    let secret: String
    let pages: Int
    let page: Int
    var squareURL: String
    var largeURL: String
}

extension FlickrObject {
    init?(json: [String: Any]) {
        guard let farmInt = json["farm"] as? Int,
            let server = json["server"] as? String,
            let photoID = json["id"] as? String,
            let secret = json["secret"] as? String,
            let tag = json["tag"] as? String,
            let pages = json["pages"] as? Int,
            let page = json["page"] as? Int
        else {
            return nil
        }
        
        let farm = String(farmInt)
        let sqrURL = "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_q.jpg"
        let lrgURL = "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_b.jpg"
        
        self.tag = tag
        self.farm = farm
        self.server = server
        self.photoID = photoID
        self.secret = secret
        self.pages = pages
        self.page = page
        self.squareURL = sqrURL
        self.largeURL = lrgURL
    }
}

struct Certificates {
  static let flickr = Certificates.certificate(filename: "flickr.com")
  
  private static func certificate(filename: String) -> SecCertificate {
    let filePath = Bundle.main.path(forResource: filename, ofType: "der")!
    let data = try! Data(contentsOf: URL(fileURLWithPath: filePath))
    let certificate = SecCertificateCreateWithData(nil, data as CFData)!
    
    return certificate
  }
}


class FlickrHelper {
    
    // MARK: SSLPinning
    
    var onComplete: ((_ result: Array<FlickrObject>)->())?
    
    let evaluators = [
      "api.flickr.com":
        PinnedCertificatesTrustEvaluator(certificates: [
          Certificates.flickr
        ])
    ]

    let session: Session

    init() {
      session = Session(
        serverTrustManager: ServerTrustManager(evaluators: evaluators)
      )
    }
    
    // MARK: NetworkHelper
    
    func startQueryFor(searchTag: String, page: Int) {
        
        let urlString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=\(searchTag)&page=\(String(page))&format=json&nojsoncallback=1"
        let urlQuery: URL = URL(string: urlString)!
        
        session.request(urlQuery).response { result in
            if let data = result.data, let _ = result.request?.url {
                    
                    let jsonData = data
                    if !jsonData.isEmpty {
                        self.parseDataWith(data: jsonData, tag: searchTag)
                    }
                }
            }
        
    }
    
    func parseDataWith(data: Data, tag: String) {
        do {
            var imageArray: Array<FlickrObject> = []
            
            let response = try JSONSerialization.jsonObject(with: data) as! Dictionary<String, AnyObject>
            let json = response["photos"] as! Dictionary<String, AnyObject>
            let photoArray = json["photo"] as! Array<Dictionary<String, AnyObject>>

            for photoDict in photoArray {
                var modDict = photoDict
                modDict["pages"] = json["pages"] as AnyObject
                modDict["page"] = json["page"] as AnyObject
                modDict["tag"] = tag as AnyObject
                
                if let image = FlickrObject(json: modDict){
                    imageArray.append(image)
                }
            }
            
            DispatchQueue.main.async {
                self.onComplete?(imageArray)
            }
        } catch {
            print("error")
        }
    }

    
}
