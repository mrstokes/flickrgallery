//
//  ImageViewCell.swift
//  PhotoGallery
//
//  Created by Chris Stokes on 15/03/2020.
//  Copyright © 2020 Chris Stokes. All rights reserved.
//

import UIKit
import AlamofireImage

class ImageViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}
