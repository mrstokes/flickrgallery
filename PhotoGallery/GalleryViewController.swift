//
//  GalleryViewController.swift
//  PhotoGallery
//
//  Created by Chris Stokes on 14/03/2020.
//  Copyright © 2020 Chris Stokes. All rights reserved.
//

import UIKit
import Kingfisher

private let reuseIdentifier = "PhotoCell"


class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageOverlay: UIImageView!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    let flickrHelper = FlickrHelper()
    let safeCharacters = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    var imageArray: Array<FlickrObject> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageOverlay.isHidden = true
        imageOverlay.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.9)
        imageOverlay.addGestureRecognizer(tapGesture)
        
        flickrHelper.onComplete = { result in
            if self.imageArray.count > 0 {
                if self.imageArray[0].tag == result[0].tag {
                    self.imageArray.append(contentsOf: result)
                } else {
                    self.imageArray = result
                }
            } else {
                self.imageArray.append(contentsOf: result)
            }
            
            self.collectionView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageViewCell
    
        let photoData = imageArray[indexPath.row]
        let url = URL(string: photoData.squareURL)!
        
        cell.activityIndicator.startAnimating()

        cell.cellImageView.af.setImage(withURL: url) { result in
            if (result.error != nil) {
                print(result.error!)
            } else {
                cell.activityIndicator.stopAnimating()
            }
        }
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if imageArray.count > 1 {
            if indexPath.row == imageArray.count - 1 {
                let object = imageArray[indexPath.row]
                let nextPage = object.page + 1
                
                if nextPage <= object.pages {
                    flickrHelper.startQueryFor(searchTag: object.tag, page: nextPage)
                }
            }
        }
        
    }

    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let largeImage = imageArray[indexPath.row]
        let url = URL(string: largeImage.largeURL)
        
        imageOverlay.isHidden = false
        
        imageOverlay.kf.indicatorType = .activity
        imageOverlay.kf.setImage(with: url)
        
        largeImageViewActive(active: true)
        tapGesture.isEnabled = true
    }
    
    
    // MARK: UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (layout?.minimumInteritemSpacing ?? 0.0) + (layout?.sectionInset.left ?? 0.0) + (layout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        
        return CGSize(width: size, height: size)
    }
    
    
    // MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        var searchText = self.searchBar.text
        
        searchText = removeCharactersFromTag(text: searchText!, set: safeCharacters)
        
        if (searchText!.count < 2) {
            searchText = "Kitten"
        }
        
        self.searchBar.text = searchText
        
        flickrHelper.startQueryFor(searchTag: searchText!, page: 1)
    }
    
    
    // MARK: MyFunctions
    
    func removeCharactersFromTag(text: String, set characterSet: Set<Character>) -> String {
        return String(text.filter { characterSet.contains($0) })
    }
    
    func largeImageViewActive(active: Bool) {
        searchBar.isUserInteractionEnabled = !active
        collectionView.isUserInteractionEnabled = !active
    }
    
    @IBAction func imageTapped(_ sender: Any) {
        imageOverlay.isHidden = true
        largeImageViewActive(active: false)
        tapGesture.isEnabled = false
    }
    

}
